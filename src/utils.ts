/** Parse date in format DD.MM. HH:SS */
export function parseShortHandDate(date: string): Date {
    const separator = /\s*[\.:]\s*/
    const [dayString, monthString, hourString, minuteString] = date.split(separator);
    return new Date(2022, (+monthString) - 1, +dayString, +hourString, +minuteString);
}

export function timeout(ms: number): Promise<unknown> {
    return new Promise(resolve => setTimeout(resolve, ms));
}
