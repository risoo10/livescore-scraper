import { parseShortHandDate } from "./utils";

test('parse "06.05. 13:20" to Date', () => {
    expect(parseShortHandDate('06.05. 13:20')).toEqual(new Date(2022, 4, 6, 13, 20));
});