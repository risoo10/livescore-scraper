import * as puppeteer from "puppeteer";
import * as cheerio from 'cheerio';
import * as fs from "fs";
import { parseShortHandDate, timeout } from "./utils";

const baseUrl = 'https://www.flashscore.sk/hokej/svet/majstrovstva-sveta';


interface TournamentTable {
    name: string;
    standings: TournamentStanding[];
}

interface TournamentStanding {
    rank: number;
    participant: string;
    participantUrl: string;
    points: number;
}

interface TournamentMatch {
    matchStart: Date | string;
    playerHome: string;
    playerAway: string;
    scoreHome?: number;
    scoreAway?: number;
    matchUrl: string;
}



async function loadPageContent(browser, url): Promise<string> {
    console.log(`Loading page [${url}] ...`);
    const page = await browser.newPage();
    await page.goto(url);
    const content = await page.content();
    console.log(`  - Loaded`);
    return content;
}

async function parseTournamentTables(browser: puppeteer.Browser): Promise<TournamentTable[]> {

    const content = await loadPageContent(browser, `${baseUrl}/`)
    
    const tournamentTables: TournamentTable[] = [];
    const $ = cheerio.load(content);

    const tournamentTable = $("#tournament-table").first();
    const tables = tournamentTable.find(".ui-table").toArray();
    tables.forEach((table) => {
        const tableNode = $(table)
        const tableHeader = tableNode.find(".table__headerCell--participant").first();
        const newTable: TournamentTable = {
            name: tableHeader?.text() || "Unknown table name",
            standings: [],
        };
        tableNode.find(".ui-table__row").each((index, row) => {
            const rowNode = $(row);
            const cellRank = rowNode.find(".tableCellRank").first()
            const cellParticipantName = rowNode.find(".tableCellParticipant__name").first();
            const cellPoints = rowNode.find(".table__cell--points").first();

            const participantEntry: TournamentStanding = {
                participant: cellParticipantName?.text() || 'Unknown Participant',
                participantUrl: `https://www.flashscore.sk${cellParticipantName?.attr("href")}` || "Unknown Participant url",
                rank: +(cellRank?.text() ?? undefined),
                points: +(cellPoints?.text() ?? undefined),
            }

            newTable.standings.push(participantEntry);
        })

        tournamentTables.push(newTable);
    })

    return tournamentTables;

}

async function parseTournamentSchedule(browser: puppeteer.Browser): Promise<TournamentMatch[]> {

    const content = await loadPageContent(browser, `${baseUrl}/program`);
    
    const $ = cheerio.load(content);
    const liveTable = $('#live-table').first();
    const eventMatches = liveTable.find('.event__match').toArray();
    
    return eventMatches.map((matchElement) => {
        const match = $(matchElement);
        const [matchId] = match.attr('id').split('_').slice(-1);
        const timeElem = match.find('.event__time').first();
        const homePlayer = match.find('.event__participant--home').first();
        const awayPlayer = match.find('.event__participant--away').first();
        const homeScore = match.find('.event__score--home').first();
        const awayScore = match.find('.event__score--away').first();
        return {
            matchStart: timeElem?.text() ? parseShortHandDate(timeElem.text()): undefined,
            playerHome: homePlayer?.text(),
            playerAway: awayPlayer?.text(),
            scoreHome: +(homeScore?.text() ?? undefined), 
            scoreAway: +(awayScore?.text() ?? undefined),
            matchUrl: `https://www.flashscore.sk/zapas/${matchId}/#prehlad-zapasu`
        }
    });
}

(async () => {
    console.log("Launching Headless Browser ...");
    const browser = await puppeteer.launch();
    console.log("  - Launched");

    console.log("Parsing tournament tables ...");
    const tables = await parseTournamentTables(browser);
    console.log("  - Done")
    
    console.log("Parsing tournament matches ...");
    const matches = await parseTournamentSchedule(browser);
    console.log("  - Done")
    

    console.log("Wrtinging to file [tournament.json]");
    fs.writeFileSync('tournament.json', JSON.stringify({tables, matches}));

    await browser.close();
})();

